package main

import (
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"url-collector/controller"
)

func main() {
	controller.Register()
	godotenv.Load(".env")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
