package response

import (
	"encoding/json"
	"net/http"
	"url-collector/model"
)

type PictureResponseWriter struct {
}

func (p *PictureResponseWriter) Write(w http.ResponseWriter, pods []model.POD) error {
	podUrls := make([]string, 0)

	for _, pod := range pods {
		podUrls = append(podUrls, pod.Url)
	}
	w.Header().Add("Content-Type", "application-json")

	err := json.NewEncoder(w).Encode(map[string][]string{"urls": podUrls})
	if err != nil {
		return err
	}

	return nil
}
