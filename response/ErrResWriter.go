package response

import (
	"encoding/json"
	"log"
	"net/http"
)

type ErrResWriter struct {
}

func (e *ErrResWriter) Write(w http.ResponseWriter, statusCode int, msg string) {
	w.Header().Add("Content-Type", "application-json")

	w.WriteHeader(statusCode)

	err := json.NewEncoder(w).Encode(map[string]string{"error": msg})
	if err != nil {
		http.Error(w, "Application Error", 500)
		log.Fatal(err)
	}
}
