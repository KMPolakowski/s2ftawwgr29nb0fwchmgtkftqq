package testutils

import (
	"errors"
	"time"
	"url-collector/model"
)

type ClientMock struct {
	Stub    []string
	WantErr bool
}

func (c *ClientMock) GetPod(time time.Time) (model.POD, error) {
	if c.WantErr {
		return model.POD{}, errors.New("Err")
	}

	if len(c.Stub) == 0 {
		return model.POD{}, nil
	}

	pod := model.POD{
		Url: c.Stub[0],
	}
	c.Stub = c.Stub[1:]

	return pod, nil
}

type ReqMock struct {
	FromString  string
	ToString    string
	WantValdErr bool
}

func (r ReqMock) Validate() error {
	if r.WantValdErr {
		return errors.New("Vald Err")
	}

	return nil
}

func (r ReqMock) From() time.Time {
	t, err := time.Parse("2006-01-02", r.FromString)
	if err != nil {
		panic("Cant parse from string")
	}
	return t
}

func (r ReqMock) To() time.Time {
	t, err := time.Parse("2006-01-02", r.ToString)
	if err != nil {
		panic("Cant parse to string")
	}
	return t
}
