FROM golang:1.17-alpine

WORKDIR /url-collector
COPY . .

RUN go build .
EXPOSE 8080

CMD ["./url-collector"]