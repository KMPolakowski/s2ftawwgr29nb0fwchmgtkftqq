package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"url-collector/interfaces"
	"url-collector/model"
	service2 "url-collector/service"
	"url-collector/testutils"
)

func Test_FetchUrl(t *testing.T) {
	tests := []struct {
		name     string
		service  service2.PictureService
		req      interfaces.PictureRequest
		expected []model.POD
		wantErr  bool
	}{
		{
			name: "Works for 4 days",
			service: service2.PictureService{
				Client: &testutils.ClientMock{Stub: []string{"1", "2", "3", "4"},
					WantErr: false},
			},
			req: testutils.ReqMock{
				FromString: "2021-12-11",
				ToString:   "2021-12-14",
			},
			expected: []model.POD{
				{Url: "1"},
				{Url: "2"},
				{Url: "3"},
				{Url: "4"},
			},
			wantErr: false,
		},
		{
			name: "Client error",
			service: service2.PictureService{
				Client: &testutils.ClientMock{Stub: []string{"1", "2", "3", "4"}, WantErr: true},
			},
			req: testutils.ReqMock{
				FromString: "2021-12-11",
				ToString:   "2021-12-14",
			},
			expected: []model.POD{},
			wantErr:  true,
		},
		{
			name: "Works for 1 day",
			service: service2.PictureService{
				Client: &testutils.ClientMock{Stub: []string{"https://nice.url"}},
			},
			req: testutils.ReqMock{
				FromString: "2021-12-11",
				ToString:   "2021-12-11",
			},
			expected: []model.POD{{Url: "https://nice.url"}},
			wantErr:  false,
		},
		{
			name: "Err when client returns no PODs",
			service: service2.PictureService{
				Client: &testutils.ClientMock{Stub: []string{}},
			},
			req: testutils.ReqMock{
				FromString: "2021-12-11",
				ToString:   "2021-12-11",
			},
			expected: []model.POD{},
			wantErr:  true,
		},
		{
			name: "Err when client returns POD with empty url",
			service: service2.PictureService{
				Client: &testutils.ClientMock{Stub: []string{""}},
			},
			req: testutils.ReqMock{
				FromString: "2021-12-11",
				ToString:   "2021-12-11",
			},
			expected: []model.POD{},
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pods, err := tt.service.FetchUrls(tt.req)

			assert.Equal(t, tt.wantErr, err != nil, "Expected error: %v, got %v", tt.wantErr, err)
			if err == nil {
				assert.Equal(
					t,
					len(tt.expected),
					len(pods),
					"Expected pods len %v got %v instead",
					len(tt.expected),
					len(pods),
				)
				for i, v := range tt.expected {
					assert.Equal(t, v, pods[i], "Expected pod url %v got %v instead", v, pods[i])
				}
			}
		})
	}
}
