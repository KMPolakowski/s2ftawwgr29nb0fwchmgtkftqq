package controller

import (
	"net/http"
	"url-collector/interfaces"
	"url-collector/request"
	"url-collector/response"
	"url-collector/service"
)

type Controller struct {
	Service      interfaces.PictureService
	Req          interfaces.PictureRequest
	ResWriter    interfaces.PictureResponseWriter
	ErrResWriter interfaces.ErrResponseWriter
}

func Register() {
	http.HandleFunc("/pictures", Controller{}.picturesHandler)
}

func (c Controller) picturesHandler(w http.ResponseWriter, r *http.Request) {
	if c.Req == nil {
		c.Req = &request.PictureRequest{R: r}
	}

	if c.Service == nil {
		c.Service = &service.PictureService{}
	}

	if c.ResWriter == nil {
		c.ResWriter = &response.PictureResponseWriter{}
	}

	if c.ErrResWriter == nil {
		c.ErrResWriter = &response.ErrResWriter{}
	}

	err := c.Req.Validate()
	if err != nil {
		c.ErrResWriter.Write(w, 422, err.Error())
		return
	}

	pods, err := c.Service.FetchUrls(c.Req)
	if err != nil {
		c.ErrResWriter.Write(w, 500, err.Error())
		return
	}

	err = c.ResWriter.Write(w, pods)
	if err != nil {
		c.ErrResWriter.Write(w, 500, err.Error())
		return
	}
}
