## URL COLLECTOR

# Request Sample

```
GET /pictures?from=2021-12-12&to=2021-12-14
```

# Response Sample

```json
{
  "urls": [
    "https://apod.nasa.gov/apod/image/2112/M3Leonard_Bartlett_960.jpg",
    "https://apod.nasa.gov/apod/image/2112/auroraemeteors_boardman_1080.jpg",
    "https://apod.nasa.gov/apod/image/2112/HH666_HubbleOzsarac_960.jpg"
  ]
}
```

# DEPLOYMENT

On repo root:

```
docker image build . --tag url-collector
```

then:

```
docker run -p 8080:8080 -d --name url-collector url-collector
```

# RUN TESTS

```
go test .
```