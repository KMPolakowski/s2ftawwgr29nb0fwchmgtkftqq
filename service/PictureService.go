package service

import (
	"errors"
	"fmt"
	"golang.org/x/sync/errgroup"
	"log"
	"os"
	"strconv"
	"time"
	"url-collector/client"
	interfaces "url-collector/interfaces"
	"url-collector/model"
)

type PodClient interface {
	GetPod(time time.Time) (model.POD, error)
}

type PictureService struct {
	Client  PodClient
	nConReq int
}

func (p *PictureService) FetchUrls(request interfaces.PictureRequest) (pods []model.POD, err error) {
	if p.Client == nil {
		p.Client = &client.NasaClient{}
	}
	if p.nConReq == 0 {
		p.nConReq, err = strconv.Atoi(os.Getenv("CONCURRENT_REQUESTS"))
		if err != nil {
			p.nConReq = 5
			log.Println(fmt.Errorf("CONCURRENT_REQUESTS not defined %v", err))
		}
	}

	var sem = make(chan int, 5)
	var podChan = make(chan model.POD)
	eg := new(errgroup.Group)

	for i := request.From(); !i.After(request.To()); i = i.AddDate(0, 0, 1) {
		sem <- 1
		i := i
		eg.Go(func() error {
			defer func() {
				<-sem
			}()

			pod, err := p.Client.GetPod(i)
			if err != nil {
				return err
			}
			if pod.Url == "" {
				return errors.New("Got empty url back")
			}
			podChan <- pod

			return nil
		})
	}

	go func() {
		eg.Wait()
		close(podChan)
		close(sem)
	}()

	//var pods = make([]model.POD, 0)

	for pod := range podChan {
		pods = append(pods, pod)
	}

	err = eg.Wait()

	return pods, err
}
