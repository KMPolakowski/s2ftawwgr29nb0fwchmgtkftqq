package client

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
	"url-collector/model"
)

type NasaClient struct {
	Client *http.Client
	url    string
	apiKey string
}

func (n *NasaClient) GetPod(t time.Time) (model.POD, error) {
	if n.Client == nil {
		n.Client = &http.Client{Timeout: 5 * time.Second}
	}

	if n.url == "" {
		n.url = os.Getenv("API_URL")
	}

	if n.apiKey == "" {
		n.apiKey = os.Getenv("API_KEY")

		if n.apiKey == "" {
			n.apiKey = "DEMO_KEY"
			log.Println(fmt.Errorf("API_KEY not defined"))
		}
	}

	res, err := n.Client.Get(n.url + "?api_key=" + n.apiKey + "&date=" + t.Format("2006-01-02"))
	if err != nil {
		return model.POD{}, err
	}

	defer res.Body.Close()
	pod := model.POD{}
	err = json.NewDecoder(res.Body).Decode(&pod)
	if err != nil {
		return model.POD{}, err
	}

	return pod, nil
}
