package request

import (
	"errors"
	"net/http"
	"time"
)

type PictureRequest struct {
	R    *http.Request
	from time.Time
	to   time.Time
}

func (req *PictureRequest) Validate() error {
	query := req.R.URL.Query()

	if !query.Has("from") && !query.Has("to") {
		return errors.New("Missing from and to selection")
	}

	from, err := time.Parse("2006-01-02", query.Get("from"))
	if err != nil {
		return errors.New("Invalid from date")
	}

	to, err := time.Parse("2006-01-02", query.Get("to"))
	if err != nil {
		return errors.New("Invalid from date")
	}

	if to.After(time.Now()) {
		return errors.New("To date cannot be after today")
	}

	if from.After(to) {
		return errors.New("From date cannot be after the to date")
	}

	req.from = from
	req.to = to

	return nil
}

func (req PictureRequest) From() time.Time {
	return req.from
}

func (req PictureRequest) To() time.Time {
	return req.to
}
