package interfaces

import (
	"net/http"
	"time"
	"url-collector/model"
)

type PictureService interface {
	FetchUrls(request PictureRequest) ([]model.POD, error)
}

type PictureRequest interface {
	Validate() error
	From() time.Time
	To() time.Time
}

type PictureResponseWriter interface {
	Write(w http.ResponseWriter, pod []model.POD) error
}

type ErrResponseWriter interface {
	Write(w http.ResponseWriter, statusCode int, msg string)
}
